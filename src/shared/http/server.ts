import 'reflect-metadata';
import 'dotenv/config';
import express, { NextFunction, Request, Response } from 'express';
import 'express-async-errors';
import cors from 'cors';
import { errors } from 'celebrate';
import routes from '../routes/index';
import AppError from '../errors/AppError';
import '../typeorm';
import uploadConfig from '../../config/upload';
import rateLimiter from '../middlewares/rateLimiter';

const app = express();

app.use(cors());
app.use(express.json());

app.use('/files', express.static(uploadConfig.directory));
app.use(routes);

app.use(errors());
app.use(rateLimiter);

app.use(
  (error: Error, request: Request, response: Response, next: NextFunction) => {
    if (error instanceof AppError) {
      return response.status(error.statusCode).json({
        status: 'error',
        message: error.message,
      });
    }

    console.log(error);

    return response.status(500).json({
      status: 'error',
      message: 'Internal Error ',
    });
  },
);

app.listen(3333, () => {
  console.log('API rodando na porta 3333  🏆 ');
});
