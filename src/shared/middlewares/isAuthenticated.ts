import { NextFunction, Request, Response } from 'express';
import AppError from '../errors/AppError';
import { verify } from 'jsonwebtoken';
import authConfig from '../../config/auth';

interface TokenPayLoad {
  userID: string;
  iat: number;
  exp: number;
}

export default function is_Authenticated(
  req: Request,
  res: Response,
  next: NextFunction,
): void {
  const authHeader = req.headers.authorization;

  if (!authHeader) {
    throw new AppError('Não há um token habilitado');
  }

  const [, token] = authHeader.split(' ');
  try {
    const decodedToken = verify(token, authConfig.jwt.secret);

    const { userID } = decodedToken as TokenPayLoad;

    req.user = {
      id: userID,
    };

    return next();
  } catch (error) {
    throw new AppError('Token inválido');
  }
}
