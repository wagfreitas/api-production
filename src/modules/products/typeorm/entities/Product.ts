import { generateKeyPair } from 'crypto';
import OrdersProducts from '../../../orders/typeorm/entities/OrdersProducts';
import productsRouter from '../../routes/products.routes';
import {
  Column,
  CreateDateColumn,
  Entity,
  Generated,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('products')
class Product {
  @PrimaryGeneratedColumn('uuid')
  @Generated('uuid')
  id: string;

  @OneToMany(() => OrdersProducts, order_products => order_products.product)
  order_products: OrdersProducts[];

  @Column()
  name: string;
  @Column('decimal')
  price: number;
  @Column('int')
  quantity: number;
  @CreateDateColumn()
  created_at: Date;
  @CreateDateColumn()
  update_at: Date;
}

export default Product;
