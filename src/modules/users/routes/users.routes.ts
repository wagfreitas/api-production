import { Router } from 'express';
import { celebrate, Joi, Segments } from 'celebrate';
import UsersController from '../controller/UsersController';
import is_Authenticated from '../../../shared/middlewares/isAuthenticated';
import multer from 'multer';
import uploadConfig from '../../../config/upload';
import UsersAvatarControllers from '../controller/UsersAvatarControllers';

const usersRouter = Router();
const usersController = new UsersController();
const usersAvatarController = new UsersAvatarControllers();
const upload = multer(uploadConfig);

usersRouter.get('/', is_Authenticated, usersController.index);
usersRouter.post(
  '/',
  celebrate({
    [Segments.BODY]: {
      name: Joi.string().required(),
      email: Joi.string().email().required(),
      password: Joi.string().required(),
    },
  }),
  usersController.create,
);

usersRouter.patch(
  '/avatar',
  is_Authenticated,
  upload.single('avatar'),
  usersAvatarController.update,
);

export default usersRouter;
