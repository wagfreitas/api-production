import AppError from '../../../shared/errors/AppError';
import path from 'path';
import fs from 'fs';
import { getCustomRepository } from 'typeorm';
import User from '../typeorm/entities/User';
import UserRepository from '../repositories/UsersRepository';
import uploadConfig from '../../../config/upload';

interface IRequest {
  user_id: string;
  avatarFilename: string;
}

class UpdateAvatarServices {
  public async execute({ user_id, avatarFilename }: IRequest): Promise<User> {
    const usersRepository = getCustomRepository(UserRepository);
    const user = await usersRepository.findById(user_id);
    if (!user) {
      throw new AppError('Usuario não encontrado');
    }

    if (user.avatar) {
      const userAvatarFilePath = path.join(uploadConfig.directory, user.avatar);
      const userAvatarFileExiste = await fs.promises.stat(userAvatarFilePath);
      if (userAvatarFileExiste) {
        await fs.promises.unlink(userAvatarFilePath);
      }
    }
    user.avatar = avatarFilename;
    await usersRepository.save(user);

    return user;
  }
}

export default UpdateAvatarServices;
