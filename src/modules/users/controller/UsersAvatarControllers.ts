import { Response, Request } from 'express';
import UpdateAvatarServices from '../services/UpdateAvatarServices';
import { classToClass } from 'class-transformer';

export default class UsersAvatarControllers {
  public async update(req: Request, res: Response): Promise<Response> {
    const updateAvatar = new UpdateAvatarServices();

    const user = updateAvatar.execute({
      user_id: req.user.id,
      avatarFilename: req.file.filename,
    });

    return res.json(classToClass(user));
  }
}
